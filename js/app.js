//app.js
var app = angular.module("agendaTelefonica",[]);
app.controller("listaTelefonicaCtrl", function($scope){
	$scope.app = "Lista Telefônica";
	$scope.contatos = [
		{nome: "Pedro", apelido: "pedra", telefone: "99999877", operadora:{nome:"vivo", codigo:"015"}},
		{nome: "Ana", apelido: "donana", telefone: "7128329456666", operadora:{nome:"claro", codigo:"021"}},
		{nome: "Maria", apelido: "lia", telefone: "3013880000", operadora:{nome:"tim", codigo:"041"}}

	];
})